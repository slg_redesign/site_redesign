// Index
// ======
// Purpose: Connect all functionality for the server elements. The file is also responsible for maintaining all routes.
// Author(s): Greg Cho
// Reviewer: None
// Date Reviewed: None:
// Comments: All global variables are set up in the setup_globals Javsacript file by the setupGlobal function.


// Set up all necessary global variables
var setup = require('./server_elements/setup/setup');
