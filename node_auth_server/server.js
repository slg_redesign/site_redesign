var setup = require('./server_elements/setup/setup');
var subroutines = require('./server_elements/subroutines/subroutines');
var path = require('path');

setup.app.post('/login', setup.auth.jwtLogin);

setup.app.post('/register', setup.auth.jwtRegistration);

setup.app.get('/jwt_payload', subroutines.authenticateToken); // as a gut decision, decided to keep the token in the header of the request as opposed to the url since I'm pretty sure the http header is encrypted. I'm not too sure if the url is as well... They're both encrypted. There's no difference in the method of token transmission

setup.app.get('/refreshed_token', setup.auth.refreshToken);

setup.app.get('/new_jwt_token', setup.auth.isLoggedIn, subroutines.changeValidation); // necessary layer of security to ensure that the request is authorized to change the user validation token

setup.app.post('/new_password', setup.auth.isLoggedIn, subroutines.changePassword);

setup.app.get('/hash/:password', setup.auth.isLoggedIn, subroutines.hashAndSend);