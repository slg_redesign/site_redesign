// Tests
// ======
// Purpose: Contains all chai unit tests for this module
// Author(s): Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None


var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('./../server');
var server_location = 'http://localhost:5050';
var setup = require('./../server_elements/setup/setup');
var request = require('supertest');
var auth = setup.auth;
var app = setup.app;

var should = chai.should();
var expect = chai.expect;

chai.use(chaiHttp);
var user = {'email': 'testuser@test.com', 'password': 'testpass', 'first_name': 'first', 'last_name': 'last'};


function clearDb() {
    var sql = 'delete from user_auth where email = ?;';
    setup.conn.query(sql, [user.email], function(err, rows, fields){
        if(err) return err;
    });
}

//registration tests
describe('authentication_tests', function(){
    //test register function
    it('should register a user on /register POST', function(done){
        this.timeout(4000);
        chai.request(server_location)
        .post('/register')
        .send(user)
        .end(function(err, response){
            should.exist(response);
            should.exist(response.body.token);
            response.should.be.json;
            response.body.message.should.equal('Registration successful.');
            user['token'] = response.body.token;
            done();
        });
    });

    it('should indicate that an already existing account attempted to register using the /register POST request', function(done) {
        this.timeout(4000);
        chai.request(server_location)
        .post('/register')
        .send(user)
        .end(function(err, response) {
            should.exist(response);
            should.exist(response.body.message);
            should.not.exist(response.body.token);
            response.body.message.should.equal('Email already used in a previous account.');
            done();
        });
    });

    it('should login a successfully registered user using the /login POST request', function(done) {
        chai.request(server_location)
        .post('/login')
        .send(user)
        .end(function(err, response) {
            should.exist(response);
            should.exist(response.body.message);
            response.should.be.json;
            response.body.message.should.equal('Login successful.');
            should.exist(response.body.token);
            user['token'] = response.body.token;
            done();
        });
    });

    it('should refuse a login attempt with incorrect information using the /login POST request', function(done) {
        chai.request(server_location)
        .post('/login')
        .send({'email': 'testuser@test.com', 'password': 'fakepass'})
        .end(function(err, response) {
            response.should.have.status(401);
            should.not.exist(response.body.token);
            done();
        });
    });

    it('should validate a legitimate token using the /jwt_payload GET request and return payload information.', function(done) {
        chai.request(server_location)
        .get('/jwt_payload')
        .set({'token': user.token})
        .end(function(err, response) {
            should.exist(response);
            response.should.have.status(200);
            should.exist(response.body.user_id);
            done();
        });
    });

    it('should invalidate an illegitimate token using the /jwt_payload GET request', function(done) {
        chai.request(server_location)
        .get('/jwt_payload')
        .set({'token': 'wrong'})
        .end(function(err, response) {
            response.should.have.status(401);
            response.body.message.should.equal('Invalid token.');
            done();
        });
    });

    it('should allow for a successful token refresh using the /refreshed_token GET request', function(done) {
        chai.request(server_location)
        .get('/refreshed_token')
        .set({'token': user.token})
        .end(function(err, response) {
            response.should.have.status(200);
            should.exist(response.body.message);
            done();
        });
    });

    it('should invalidate an illegitimate token upon the /refreshed_token GET request', function(done) {
        chai.request(server_location)
        .get('/refreshed_token')
        .set({'token': 'wrong'})
        .end(function(err, response) {
            response.should.have.status(401);
            done();
        });
    });

    it('should change validation_token value in the user table using the /new_jwt_token GET request', function(done) {
        chai.request(server_location)
        .get('/new_jwt_token')
        .set({'token': user.token})
        .end(function(err, response) {
            should.exist(response);
            response.body.message.should.equal('Validation token change successful.');
            response.body.token.should.not.equal(user.token);
            user['new_token'] = response.body.token;
            response.should.have.status(200);
            done();
        });
    });

    it('should invalidate previous token values when using the /jwt_payload GET request', function(done) {
        chai.request(server_location)
        .get('/jwt_payload')
        .set({"token": user.token})
        .end(function(err, response) {
            should.exist(response);
            response.should.have.status(401);
            response.body.message.should.equal('Invalid token.');
            done();
        });
    });

    it('should validate new token after validation token change using the /jwt_payload GET request', function(done) {
        chai.request(server_location)
        .get('/jwt_payload')
        .set({"token": user.new_token})
        .end(function(err, response) {
            should.exist(response);
            response.should.have.status(200);
            should.exist(response.body);
            done();
        });
    });

    it('should invalidated current token, change the user password, and return a new token when using the /new_password POST request.', function(done) {
        user.password = "newpass";
        chai.request(server_location)
        .post('/new_password')
        .set({"token": user.new_token})
        .send(user)
        .end(function(err, response) {
            should.exist(response);
            response.should.have.status(200);
            should.exist(response.body);
            user.token = response.body.token;
            done();
        });
    });

    it('should invalidate old token after password update using the /jwt_payload GET request', function(done) {
        chai.request(server_location)
        .get('/jwt_payload')
        .set({'token': user.new_token})
        .end(function(err, response) {
            response.should.have.status(401);
            response.body.message.should.equal('Invalid token.');
            done();
        });
    });

    it('should not allow login to occur with old password using /login POST request.', function(done) {
        user.password = "testpass";
        chai.request(server_location)
        .post('/login')
        .send(user)
        .end(function(err, response) {
            response.should.have.status(401);
            should.not.exist(response.body.token);
            done();
        });
    }); 

    it('should get hash with /hash/:password GET request.', function(done) {
        chai.request(server_location)
        .get('/hash/:password')
        .set({"token": user.token})
        .end(function(err, response) {
            should.exist(response);
            response.should.have.status(200);
            should.exist(response.body);
            should.exist(response.body.hash);
            done();
        });
    });
});

clearDb();