### README for node_auth_server ##

Purpose: Set up a service for authentication protocols to be used with minimal overhead.  
Author(s): Greg Cho  
Company: Super League Gaming  
Last Update: 10/26/2017


### Disclaimer: ###

THIS MODULE AND IT'S CONTENTS ARE FOR INTERNAL CIRCULATION ONLY. In order to ensure site security, this file must not be shared publicly in any way.


### Overview: ###

This module is responsible for handling authentication and authorization requests from any and all services that require them. The module implements an instance of an authentication class from the node_auth package. Ideally, any and all strategies with similar functionality may be swapped into this service without substantial interruption to other services.


### Usage: ###

To use this module simply type:

node index.js

into the command line. This will set up the api on a local host designated by the server (default is 5050). Any call to this api will then be sent to the entry point localhost:5050.


### End Points: ###

### POST /register ###

Inputs: Takes email, first_name, last_name, and password as arguments in the body of the https request.  
Outputs (JSON): 

```javascript
{
    "message": <"message indicating the status of the request">,
    "token": <"jwt token element (only exists upon successful POST request)">
}
```

### POST /login ###

Inputs: Takes email and password as arguments in the body of the https request.  
Outputs (JSON):  

```javascript
{
    "message": <"message indicating the status of the request">,
    "token": <"jwt token element (only exists upon successful POST request)">
}
```

### GET /validate ###

Inputs: Takes the token in the headers section of the https request. The identifyer looked for by the parser is "token".  
Outputs (JSON upon success):  

```javascript
{
    "user_id": <"user_id">,
    "email": <"email">,
    "iat": <"token initialization time">,
    "exp": <"token experation time">,
    "iss": <"issuer">
}
```

### GET /refresh_token ###

Inputs: Takes the token in the headers section in the https request.  
Outputs (JSON response on success):  

```javascript
{
    "message": <"message indicating the status of the request and the reason for success or failure">,
    "token": <"jwt token (if the request is successful)">
}
```

### GET /change_validation ###

Purpose: Adds a randomly generated token to the user table which is included in the jwt token. If the token validated maintains an invalid validation token then the JWT token is considered invalid as well. This allows the authentication service to forcibly log out users by invalidating tokens without the appropriate random string.  
Inputs: Takes the current user JWT token in the headers section of the http request.  
Outputs (JSON object):

```javascript
{
    "message": <"indicates if the validation token change was successful">,
    "token": <"new JWT token">
}
```


### Bugs: ###

There are no currently noticable bugs in this program. The server does need to create it's own database instance to track users, but otherwise it is pretty much complete.


### Testing: ###

To run all unit tests for this code simply type the command "mocha" in the terminal while in the appropriate directory.