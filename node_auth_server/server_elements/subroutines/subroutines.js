/* 
Subroutines:
============
Purpose: To handle most of the logic in the index.js file. Note that, as this service is not significantly complicated, most of this logic doesn't require compartmentalization.
Author: Greg Cho
Reviewer: None
Date Reviewed: None
Comments: None
*/ 


var setup = require('./../setup/setup');

module.exports.authenticateToken = function (request, response) {
    var token = request.headers.token;
    setup.auth.tokenPayloadAsync(token, function(payload) {
        if (payload !== null && payload !== undefined) {
            response.json(payload);
        } else {
            response.status(401);
            response.json({'message': 'Invalid token.'});
        }
    });
}


module.exports.hashAndSend = function(request, response) {
    var password = request.params.password;
    var cypher = setup.auth.hash(password);
    response.json({hash: cypher});
    return null;
}


module.exports.changeValidation = function(request, response) {
    var token = request.headers.token;
    setup.auth.changeUserValidationToken(request.user.user_id, response);
    return null;
}


module.exports.changePassword = function(request, response) {
    var user_id = request.user.user_id;
    var new_password = request.body.password;
    setup.auth.changeUserPassword(user_id, new_password, response);
    return null;
}