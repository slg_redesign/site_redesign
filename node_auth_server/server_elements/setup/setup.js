var express = require('express');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var http = require('http');

var authenticate = require('./../../../node_auth/authentication/jwt_auth');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var port = process.env.PORT || 5050;

// set up body parser options
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// options for the mysql database (edit as needed)
var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nthgames2_auth'
});

// connect to the mysql database
conn.connect(function(err) {
    if (err) throw err;
});

// set up authentication protocols using JwtAuthObj
var auth = new authenticate.JwtAuthObj(app);

// modify necessary fields
auth.modifyDbName('nthgames2_auth');
auth.modifyUserTable('user_auth');

server.listen(port, function(){
    console.log('- Server listening on given port 5050');
});

module.exports.auth = auth;
module.exports.app = app;
module.exports.server = server;
module.exports.port = port;
module.exports.conn = conn;
