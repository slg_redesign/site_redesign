// Home Server
// ===========
// Purpose: Set up home server functionality
// Author(s): Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None



// function to render home page
function renderHomePage(request, response) {
    console.log(request);
    response.render('home_test.html', {root: __dirname});
}


// export the functions in question
module.exports.renderHomePage = renderHomePage;
