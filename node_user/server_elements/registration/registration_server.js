// Registration Server
// ============
// Purpose: Handle all basic functionality for registering
// Author(s): Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None


var subroutines = require('./../subroutines/subroutines');


// render the register page
function renderRegisterPage(request, response) {
    response.render('register.html', {root: __dirname});
}

// render register redirect page
function renderRegisteredPage(request,response){
	// console.log(request);
	response.render('registered.html', {root: __dirname});
}


// funciton to get city, state, and country id's
function getIds(city, state, country, callback) {
    var sql = 'SELECT city_id FROM cities WHERE city = ?;';
}

// export modules
module.exports.renderRegisterPage = renderRegisterPage;
module.exports.renderRegisteredPage = renderRegisteredPage;
