// Setup
// ======
// Purpose: Runs the the service is started, sets up all requirements and starts server
// Author(s): Greg Cho, Chris Weber
// Reviewer: Chris Weber
// Date Reviewed: None
// Comments: None

var subroutines = require('./../subroutines/subroutines');
var mysql = require('mysql');
var express = require('express');
var engines = require('consolidate');
var bodyParser = require('body-parser');
var http = require('http');
var nodemailer = require('nodemailer');

var authenticate = require('./../../../node_auth/authentication/authenticate'); // this path may change based on how we arrange this stuff...

// App
var app = express();
var server = http.createServer(app);

    // Sockets
var io = require('socket.io').listen(server);
var port = process.env.PORT || 1234;

// Set port to listen to
server.listen(port, function(){
    console.log('- Server listening on given port 1234');
});

// options for the mysql database (edit as needed)
var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nthgames2'
});

// connect to the mysql database
conn.connect(function(err) {
    if (err) throw err;
    // console.log('Connected to MySQL database');
});

// set up body parser options
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//set up view engine
app.engine('html', engines.hogan);
app.set('views', __dirname + '/../../public/pages/html'); // locate html pages relative to setup file
app.use(express.static('public/pages')); // location of static files that express can serve

// setup authentication object
var auth = new authenticate.AuthObj(app);

var credentials = {service: 'gmail', auth: {user: 'chris.weber@superleague.com', pass: 'saphira1'}};

// setup mail transporter
var transporter = nodemailer.createTransport(credentials);

module.exports.app = app;
module.exports.conn = conn;
module.exports.server = server;
module.exports.port = port;
module.exports.io = io;
module.exports.express = express;
module.exports.auth = auth;
module.exports.nodemailer = nodemailer;
module.exports.transporter = transporter;
module.exports.credentials = credentials;
