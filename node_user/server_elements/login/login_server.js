// Login Server
// ============
// Purpose: Handle all basic functionality for login
// Author(s): Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None


// set up elements
var auth = require('./../setup/setup').auth;


// render the login page
function renderLoginPage(request, response) {
    response.render('login_test.html', {root: __dirname});
}


// function to handle login authentication (note: this function is depreciated)
// input: None
// output: None, it allows for the callback function to proceed
function handleLoginAuth(req, res, next) { // handle login failure and success events
    auth.passport.authenticate('local-login', {
        successRedirect: '/',
        failureRedirect: '/login',
        badRequestMessage: 'Username and password combination not recognized.',
        failuerFlash: true
    })(req, res, next);
}


// handle the callback for a successful login (should be handled in handleLoginAuth)
// input: request, response
// output: None (it prints some shit out though)
function successLoginCallback(request, response) {
    console.log('Received post request from login page.');
}


// export elements
module.exports.renderLoginPage = renderLoginPage;
module.exports.handleLoginAuth = handleLoginAuth;
module.exports.successLoginCallback = successLoginCallback;
