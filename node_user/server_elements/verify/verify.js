// Verify
// ======
// Purpose: Set up verification methods for user
// Author: Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None


var setup = require('./../setup/setup');


// Purpose: Checks to see that the token exists in the database
// Inputs: token
// Output: Boolean in the form of an argument for a specified callback
function tokenValid(token, user_id, callback) {
    var sql = 'SELECT email_token FROM users WHERE email_token = ?;';
    setup.conn.query(sql, [token], function(error, rows, fields) {
        if (rows == null) {
            token = setup.auth.generateToken(47);
            tokenValid(token, user_id, callback);
        }
        if (parseInt(rows.length) > 0) {
            token = setup.auth.generateToken(47);
            tokenValid(token, user_id, callback);
        } else {
            callback(token, user_id);
        }
    });
}


// Purpose: Updates token in the database
// Inputs: Token
// Outputs: Boolean to indicate query completion.
function updateTokenDB(token, user_id) {
    var sql = 'UPDATE users SET email_token = ? WHERE user_id = ?;';
    setup.conn.query(sql, [token, user_id], function(error, rows, fields) {
        if (error) throw error;
        return true;
    });
}


// Purpose: Sets a unique verification token for a new user
// Inputs: user id
// Outputs: None
function setToken(user_id) {
    var token = setup.auth.generateToken(47);
    updateTokenDB(token, user_id);
    return token;
}


// Purpose: Helper function for sendTokenEmail to setup mailing options and send back an appropriate JSON
// Input: User id, token
// Output: JSON containing mail options
function setupOptions(from, to, subject, source, user_id, token) {
    var mailOptionsHTML = {
        from: from,
        to: to,
        subject: subject,
        html: '<a href="' + source + '/verify/' + user_id + '/' + token + '">Link to Validation Page</a>'
    };
    return mailOptionsHTML;
}


// Purpose: Sends an email to specified user with the desired token containing the mail options necessary.
// Input: Request, response
// Output: None
function sendTokenEmail(request, response) {
    var user_id = request.user.user_id;
    // console.log(request.user.email);
    var token = setToken(user_id);
    var temp_source = 'http://localhost:1234'
    var mailOptions = setupOptions(setup.credentials, request.user.email, 'Account Validation', temp_source, user_id, token);
    setup.transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            throw error;
            // console.log(error);
            // response.json({'email_sent': false});
            response.redirect('/registered');
        } else {
            // console.log(mailOptions);
            // console.log('Email sent: ' + info.response);
            // response.json({'email_sent': true});
            response.redirect('/registered');
        }
    });
}


// Purpose: Verifies one users email address
// Inputs: SQL connection, user id, email_token, response object
// Outputs: JSON containing the key 'verified'.  If 'verified' = TRUE then the email address has been verified.  If FALSE then the email_token was incorrect.
function verifyEmail(user_id, email_token, response){
    //get email token from users table via user_id
    sql1 = 'SELECT email_token FROM users WHERE user_id = ?;';
    sql2 = 'UPDATE users SET is_verified = ? WHERE user_id = ?;';
    setup.conn.query(sql1, [user_id], function(err, rows, fields){
        if(err) throw err;
        //if match, then update users is_verified field
        if(email_token === rows[0].email_token){
            setup.conn.query(sql2, [true, user_id], function(err, rows, fields){
                if(err) throw err;
                response.json({'verified':true});
            })
        }
        else{
            response.json({'verified':false});
        }
    });
}


module.exports.verifyEmail = verifyEmail;
module.exports.setToken = setToken;
module.exports.sendTokenEmail = sendTokenEmail;