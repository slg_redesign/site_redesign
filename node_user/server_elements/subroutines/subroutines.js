// Subroutines
// ======
// Purpose: Contains commonly called functions
// Author(s): Greg Cho, Chris Weber
// Reviewer: Greg Cho
// Date Reviewed: 09/27/2017
// Comments: The code could definitely use more comments. Function purpose, output, and signature should be clearly defined (this is a note for Greg as well). All json elements should be clearly documented for RESTful purposes.



// Purpose: Get information on one user from the users table in the database
// Inputs: SQL connection instance, user id, response object
// Outputs: JSON containing all of the columns of a user (JSON structure documented in README)
function getOneUser(conn, user_id, response){
    var sql = 'SELECT * FROM users WHERE user_id = ?;';
    conn.query(sql, [user_id], function(err, rows, fields){
        if(err) throw err;
        response.json(rows);
    });
};


// Purpose: Check to see that the email is not already in the database
// Inputs: SQL connection, email to check, callback function
// Outputs: Calls a callback with a boolean value. TRUE means the email is in the database, FALSE means it is not
function checkEmail(conn, email, callback) {
	var sql = 'SELECT * FROM users WHERE email = ?;'
	conn.query(sql, [email], function(err, rows, fields) {
		if (err) { throw err; }
		if (rows.length > 0) {
			callback(true);
		} else {
			callback(false);
		}
	});
}


// Purpose: Deletes one user from the database
// Inputs: SQL connection, user id, response object
// Outputs: JSON containing the key 'affectedRows'.  If affectedRows > 0 then something was deleted (JSON structure documented in README)
function deactivateUser(conn, user_id, response){
    var sql = 'UPDATE users SET is_active = FALSE WHERE user_id = ?;';
    conn.query(sql, [user_id], function(err, rows, fields){
        if(err) throw err;
        response.json(rows);
    });
}


// Purpose: Edits basic profile info for one user in the database.  Edits email, phone number, first name, last name, jersey size, address, city, state, country, zip code, and date of birth
// Inputs: SQL connection, user id, request object, response object
// Outputs: JSON containing the key 'affectedRows'.  If affectedRows > 0 then something was edited (JSON structure documented in README)
function editUser(conn, user_id, request, response){
    var inputs = [request.body.email, request.body.phone_number, request.body.first_name, request.body.last_name, request.body.jersey_size, request.body.state_id, request.body.address, request.body.city_id, request.body.zip_code, request.body.country_id, request.body.date_of_birth, user_id];
    sql = 'UPDATE users SET email = ?, phone_number = ?, first_name = ?, last_name = ?, jersey_size = ?, state_id = ?, address = ?, city_id = ?, zip_code = ?, date_modified = CURRENT_TIMESTAMP, country_id = ?, date_of_birth = ? WHERE user_id = ?;';
    conn.query(sql, inputs, function(err, rows, fields){
        if(err) throw err;
        response.json(rows);
    });
}


// Purpose: Edits users SLG Profile Name in the database
// Inputs: SQL connection, user id, request object, response object
// Outputs: JSON containing the key 'affectedRows'.  If affectedRows > 0 then something was deleted (JSON structure documented in README)
function editProfileName(conn, user_id, request, response){
    sql = 'UPDATE users SET slg_profile_name = ? WHERE user_id = ?;';
    conn.query(sql, [request.body.slg_profile_name, user_id], function(err, rows, fields){
        if(err) throw err;
        response.json(rows);
    });
}

// Purpose: Edits users Account Names in the database
// Inputs: SQL connection, user id, request object, response object
// Outputs: JSON containing the key 'affectedRows'.  If affectedRows > 0 then something was deleted (JSON structure documented in README)
function editAccounts(conn, user_id, request, response){
    sql = 'UPDATE users SET minecraft_uuid = ?, summoner_name = ? WHERE user_id = ?;';
    conn.query(sql, [request.body.minecraft_uuid, request.body.summoner_name, user_id], function(err, rows, field){
        if(err) throw err;
        response.json(rows);
    });
}

module.exports.checkEmail = checkEmail;
module.exports.getOneUser = getOneUser;
module.exports.deactivateUser = deactivateUser;
module.exports.editUser = editUser;
module.exports.editProfileName = editProfileName;
module.exports.editAccounts = editAccounts;
