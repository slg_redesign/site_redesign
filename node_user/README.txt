README
======
Author(s): Greg Cho, Chris Weber
Company: Super League Gaming
Last Updated: October 8th, 2017


Overview:
=========
This User service handles all of the registration login and basic user information handling.

Use:
====
Once the git repository has been successfully cloned or pulled simply run: "npm install" to install all necessary requirements. From there the only file necessary to run the application should be server.js. The server can be started via the command:
node server.js

The app should then indicate that the app is listening on a specific port and a successful connection to the MySQL database.


Important Files:
================

server_elements:
================
Contains all server element files which are necessary for the server.js file to function.

server_elements/setup:
======================
Sets up the basic requirements for the server. This includes the necessary components for the passport local strategies which handle session creation.


Bugs:
=====
There are no noticeable bugs in this program at this time.


Tests:
======
The code can be tested via the command "npm test."  Be sure the server is not running before testing.  If you receive an error saying nyc is not found, reinstall instanbul globally with the command "npm install istanbul -g"

Endpoints:
==========

/login GET to reach the login page


/login POST to attempt to login.  Should pass in
{email: <email>,
password: <password>}


/register GET to reach register page


/register POST to attempt to register a new user.  Should pass in
{email: <email>,
password: <password>,
first_name: <first_name>,
last_name: <last_name>}


/:user_id GET to get one user from the database.  Returns JSON in the format:
[{
    'user_id': ,
    'password': , <-- Should not return this
    'last_login': ,
    'email': ,
    'is_active': ,
    'is_staff': ,
    'phone_number': ,
    'date_created': ,
    'is_verified': ,
    'email_token': ,
    'first_name': ,
    'last_name': ,
    'is_superuser': ,
    'image': ,
    'age': ,
    'jersey_size': ,
    'state_id': ,
    'address': ,
    'city_id': ,
    'zip_code': ,
    'date_modified': ,
    'date_age_recorded': ,
    'minecraft_uuid': ,
    'slg_profile_name': ,
    'affiliate_id': ,
    'country_id': ,
    'laptop_status': ,
    'promo_code': ,
    'promo_credit_balance': ,
    'date_i_agree_policy_tos': ,
    'date_of_birth': ,
    'latitude': ,
    'longitude': ,
    'is_super_action_squad': ,
    'phone_token': ,
    'phone_verified': ,
    'is_fill_player': ,
    'contact_preference': ,
    'newsletter_subscribe': ,
    'summoner_id': ,
    'summoner_name':
}]





/:user_id POST to edit a user in the database.  Should pass in
{
    'email': <value>,
    'phone_number': <value>,
    'first_name': <value>,
    'last_name': <value>,
    'jersey_size': <value>,
    'state_id': <value>,
    'address': <value>,
    'city_id': <value>,
    'zip_code': <value>,
    'country_id': <value>,
    'date_of_birth': <value>
}
Returns the same JSON as delete.


/delete/:user_id GET to delete a user from the database.  Returns JSON in the format:
{
    'fieldCount': ,
    'affectedRows': ,
    'insertId': ,
    'serverStatus': ,
    'warningCount': ,
    'message': ,
    'protocol41': ,
    'changedRows':
}
  Check the value of 'affectedRows':<value> should be 0 if nothing was deleted.


/verify/:user_id/:email_token GET to verify a users email address.  Returns JSON in the format:
{
    'verified': <boolean>
}
verified = true means it was verified.  False means the email token did not match


/edit_profile_name/:user_id POST to edit a users SLG Profile Name.  Should pass in:
{
    'slg_profile_name': <value>
}
Returns the same JSON as delete.

/edit_accounts/:user_id POST to edit a users external account names.  Should pass in:
{
    'minecraft_uuid': <value>,
    'summoner_name': <value>
}
Returns the same JSON as delete.
