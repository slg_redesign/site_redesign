// Tests
// ======
// Purpose: Contains all chai unit tests for this module
// Author(s): Greg Cho, Chris Weber
// Reviewer: None
// Date Reviewed: None
// Comments: None


var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('./../server');
var server_location = 'http://localhost:1234';
var utils = require('./util');
var setup = require('./../server_elements/setup/setup');
var request = require('supertest');
var auth = setup.auth;
var app = setup.app;

var should = chai.should();
var expect = chai.expect;

chai.use(chaiHttp);
var inputs = {'email': 'testuser@test.com', 'password': 'testpass', 'first_name': 'first', 'last_name': 'last'};

//registration tests
describe('register_tests', function(){
    //test register function
    it('should register a user on /register POST', function(done){
        this.timeout(4000);
        chai.request(server_location)
        .post('/register')
        .send(inputs)
        .end(function(err, response){
            should.exist(response);
            response.should.have.status(200);
            done();
        })
    });
});

//login tests
describe('login_tests', function() {
    //test login function
    it('should login a user on /login POST', function(done){
        chai.request(server_location)
        .post('/login')
        .send(inputs)
        .redirects(0)
        .end(function(err, response){
            should.exist(response);
            response.should.have.status(302);
            response.should.redirectTo('/');
            done();
        })
    });

    //test login fail
    it('should fail to login a user on /login POST', function(done){
        chai.request(server_location)
        .post('/login')
        .send({'email': 'testuser@test.com',
               'password': 'wrongpass',
               'first_name': 'first',
               'last_name': 'last'})

        .redirects(0)
        .end(function(err, response){
            should.exist(response);
            response.should.have.status(302);
            response.should.redirectTo('/login');
            done();
        })
    });
});

//database tests
describe('database_tests', function() {
    //after the database test, remove test users from db
    afterEach(function(done){
        var sql = 'delete from users where email = ?;';
        setup.conn.query(sql, [inputs.email], function(err, rows, fields){
            if(err) return err;
            done();
        });
    })

    // test database entries
    it('should check users datatable entry after /register POST request', function(done){
        utils.checkDB1(inputs.email, inputs.password, done);
    });
});

//User tests
describe('user_tests', function(){
    var authenticatedUser = request.agent(server_location);
    var test_user_id;

    beforeEach('add user to database', function(done){
        this.timeout(4000);
        chai.request(server_location)
        .post('/register')
        .send(inputs)
        .end(function(err, response){
            should.exist(response);
            response.should.have.status(200);
            authenticatedUser
            .post('/login')
            .send(inputs)
            .redirects(0)
            .end(function(err, response){
                should.exist(response);
                response.should.have.status(302);
                response.should.redirectTo('/');
                var sql1 = 'SELECT user_id FROM users WHERE email = ?;';
                setup.conn.query(sql1, [inputs.email], function(err, rows, fields){
                    if(err) throw err;
                    test_user_id = rows[0].user_id;
                    var sql2 = 'UPDATE users SET email_token = ? WHERE user_id =?;';
                    setup.conn.query(sql2, ['sampleToken',test_user_id], function(err, rows, fields){
                        if(err) throw err;
                        done();
                    });
                });
            });
        });


    });

    afterEach(function(done){
        var sql = 'DELETE FROM users WHERE email =?;';
        setup.conn.query(sql,[inputs.email],function(err, rows, fields){
            if(err) throw err;
            done();
        });
    });

    //get one user from the db
    it('should get a user from the database via /:user_id GET.', function(done){
        authenticatedUser
        .get('/'+test_user_id)
        .end(function(err, response){
            //console.log({response});
            //response.should.be.json;
            response.body[0].email.should.equal('testuser@test.com');
            done();
        });
    });
    //delete user from the db
    it('should deactivate a user from the database via /deactivate/:user_id GET', function(done){
        authenticatedUser
        .get('/deactivate/'+test_user_id)
        .end(function(err, response){
            response.should.be.json;
            expect(response.body.affectedRows).to.be.above(0);
            done();
        });
    })
    //edit a users info
    it('should edit a users info in the database via /:user_id POST', function(done){
        authenticatedUser
        .post('/'+test_user_id)
        .send({'email': 'testuser@test.com',
               'phone_number': '1234567890',
               'first_name': 'first',
               'last_name': 'last',
               'jersey_size': null,
               'state_id': null,
               'address': null,
               'city_id': null,
               'zip_code': null,
               'country_id': null,
               'date_of_birth': null})
        .end(function(err, response){
            utils.checkDB2(test_user_id, done);
        });
    });
    //verify email
    it('should verify a users email via /verify/:user_id/:email_token GET', function(done){
        this.timeout(4000);
        chai.request(server_location)
        .get('/verify/'+test_user_id+'/sampleToken')
        .end(function(err,response){
            response.should.be.json;
            expect(response.body.verified).to.be.true;
            done();
        });
    });
    //verify email to fail
    it('should fail to verify a users email via /verify/:user_id/:email_token GET', function(done){
        this.timeout(4000);
        authenticatedUser
        .get('/verify/'+test_user_id+'/wrongToken')
        .end(function(err,response){
            response.should.be.json;
            expect(response.body.verified).to.be.false;
            done();
        });
    });
    //edit SLG Profile name
    it('should edit a users slg profile name via /edit_profile_name/:user_id', function(done){
        authenticatedUser
        .post('/edit_profile_name/'+test_user_id)
        .send({'slg_profile_name':'sampleProfileName'})
        .end(function(err,response){
            response.should.be.json;
            expect(response.body.affectedRows).to.be.above(0);
            done();
        });
    });
    //edit account names
    it('should edit minecraft_uuid and summoner_name via /edit_accounts/:user_id', function(done){
        authenticatedUser
        .post('/edit_accounts/'+test_user_id)
        .send({'minecraft_uuid':'sampleMC_uuid',
               'summoner_name':'sampleSummonerName'})
        .end(function(err,response){
            response.should.be.json;
            expect(response.body.affectedRows).to.be.above(0);
            done();
        });
    });

});
