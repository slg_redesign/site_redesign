//Functions specific for testing

var setup = require('./../server_elements/setup/setup');


function checkDB1(email, password, done) {
	var sql = 'SELECT password FROM users WHERE email = ?;';
	setup.conn.query(sql, [email], function(err, rows, fields) {
		if (err) {return err;}
		if (rows[0].password === password) {
			console.log('password not properly hashed');
			return null;
		}
		if (rows[0].password == setup.auth.hash(password, 10)) {
			console.log('password does not contain a sufficient salt');
			return null;
		}
		if (rows[0].password == setup.auth.hash(password, 12)) {
			console.log('Issue with hash function. Insufficient entropy.');
			return null;
		}
		done();
	});
}

function checkDB2(user_id, done){
	var sql = 'SELECT phone_number FROM users WHERE user_id = ?;';
	setup.conn.query(sql, [user_id], function(err, rows, fields){
		if(err) return err;
		if(rows[0].phone_number==null){
			console.log('phone number not updated');
			return null;
		}
		done();
	});
}
module.exports.checkDB1 = checkDB1;
module.exports.checkDB2 = checkDB2;
