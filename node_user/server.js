// Index
// ======
// Purpose: Connect all functionality for the server elements. The file is also responsible for maintaining all routes.
// Author(s): Greg Cho, Chris Weber
// Reviewer: Greg Cho
// Date Reviewed: 09/27/2017
// Comments: All requirements are set up in  ./server_elements/setup/setup.js. Minor stylistic inconsistencies with some routes, otherwise excellent code. Could use more commenting.


var setup = require('./server_elements/setup/setup');
var subroutines = require('./server_elements/subroutines/subroutines');
var verify = require('./server_elements/verify/verify');


// set up all routes
// login routes
var login_server = require('./server_elements/login/login_server'); // location of login server
setup.app.get('/login', setup.auth.notLoggedIn, login_server.renderLoginPage); // handle login get request
setup.app.post('/login', setup.auth.passport.authenticate('local-login', {
        successRedirect: '/',
        failureRedirect: '/login',
        badRequestMessage: 'Username and password combination not recognized.',
        failureFlash: true
    }), login_server.successLoginCallback); // handle login post request

//register routes
var register_server = require('./server_elements/registration/registration_server');
setup.app.get('/register', setup.auth.notLoggedIn, register_server.renderRegisterPage);
setup.app.post('/register', setup.auth.passport.authenticate('local-signup',{
	failureRedirect: '/register',
	badRequestMessage: 'Error logging you in. My bad bro.',
	failureFlash: true
}), verify.sendTokenEmail);

//register redirect route
setup.app.get('/registered', setup.auth.isLoggedIn, register_server.renderRegisteredPage);

//home page
var home_server = require('./server_elements/home/home_server');
setup.app.get('/', setup.auth.isLoggedIn, home_server.renderHomePage);

//User Routes
//get one user from db
setup.app.get('/:user_id', setup.auth.isLoggedIn, function(request, response){
    // console.log("Request =" +request);
    subroutines.getOneUser(setup.conn, request.params.user_id, response);
});

//edit user info
setup.app.post('/:user_id', setup.auth.isLoggedIn, function(request, response){
    subroutines.editUser(setup.conn, request.params.user_id, request, response);
});

//remove one user from db
setup.app.get('/deactivate/:user_id', setup.auth.isLoggedIn, function(request, response){
    subroutines.deactivateUser(setup.conn, request.params.user_id, response);
});


//verify email
setup.app.get('/verify/:user_id/:email_token', function(request, response){
    verify.verifyEmail(request.params.user_id, request.params.email_token, response);
});

//update slg profile name
setup.app.post('/edit_profile_name/:user_id', setup.auth.isLoggedIn, function(request, response){
    subroutines.editProfileName(setup.conn, request.params.user_id, request, response);
});

//update minecraft_uuid and summoner_name
setup.app.post('/edit_accounts/:user_id', setup.auth.isLoggedIn, function(request, response){
    subroutines.editAccounts(setup.conn, request.params.user_id, request, response);
});
