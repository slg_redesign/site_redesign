module.exports = function Ticketing(name, event_id, max_tickets, price, sold_out = false){
    this.ticketing_name = name;
    this.event_id = event_id;
    this.max_tickets = max_tickets;
    this.price = price;
    this.sold_out = sold_out;

    this.create = function(conn){
        console.log("inserting");
        sql = 'INSERT INTO ticketing (ticketing_name, event_id, max_tickets, ticket_price, sold_out) VALUES (?,?,?,?,?);'
        conn.query(sql, [this.ticketing_name, this.event_id, this.max_tickets, this.price, this.sold_out], function(err, rows, fields){
            if(err) throw err;
        });
    }
}
