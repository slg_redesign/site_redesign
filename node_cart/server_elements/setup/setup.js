var express = require('express'),
    mysql = require('mysql'),
    bodyParser = require('body-parser'),
    http = require('http'),
    cookieParser = require('cookie-parser'),
    session = require('express-session');

var app = express(),
    server = http.createServer(app),
    io = require('socket.io').listen(server),
    port = process.env.PORT || 1234;

server.listen(port, function(){
    console.log('Sever listening on given port 1234');
});

var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nthgames2'
});

conn.connect(function(err){
    if(err) throw err;
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({ secret: 'sampleToken', cookie: {maxAge: 60000}}));

module.exports.app = app;
module.exports.conn = conn;
module.exports.server = server;
module.exports.port = port;
module.exports.io = io;
module.exports.express = express;
module.exports.session = session;
