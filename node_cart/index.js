///SWITCHING TO WOOCOMMERCE, THIS IS IRELEVENT



var setup = require('./server_elements/setup/setup');
var Cart = require('./server_elements/models/cart');
var Ticketing = require('./server_elements/models/ticketing');

//make a new ticketing object.  Takes in name, event_id, max_tickets, price, sold_out(optional)
setup.app.post('/createTicketing', function(request, response){
    if(request.body.sold_out){
        var ticketing = new Ticketing(request.body.name, request.body.event_id, request.body.max_tickets, request.body.price, request.body.sold_out);
    }
    else{
        var ticketing = new Ticketing(request.body.name, request.body.event_id, request.body.max_tickets, request.body.price);
    }
    ticketing.create(setup.conn);
    response.send('');
});

setup.app.get('/', function(request, response){
    //make a new card and save it to the db
    var cart = new Cart();
    // cart.toArray();
    request.session.cart = cart;
    response.send('');
});

setup.app.get('/cart', function(request, response){
    var cart = request.session.cart;
    var formattedArray = cart.toArray();
    response.json(cart);
})
