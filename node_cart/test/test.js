var chai = require('chai');
var chaiHttp = require('chai-http');
var server_location = 'http://localhost:1234';
var should = chai.should();
var expect = chai.expect;

chai.use(chaiHttp);

describe('Ticketing tests', function(){
    it('create a ticketing object', function(done){
        chai.request(server_location)
        .post('/createTicketing')
        .send({
                'name':'sampleName'
             })
        .end(function(err, response){
            should.exist(response);
            response.should.have.status(200);
            done();
        })
    })
})
