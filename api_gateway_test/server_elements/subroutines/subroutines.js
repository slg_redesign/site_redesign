/*
Subroutines
===========
Purpose: Set up subroutines
Author(s): Greg Cho
Reviewer(s): None
Date Reviewed: None
Notes: None
*/


var request = require('request');
var http = require('http');
var setup = require('./../setup/setup');


/*
Purpose: handle the login response from request
Inputs: error, response, body 
Outpus: None
*/
function handleLoginResponse(error, response, body) {
    if (error) throw error;
    if (response.statusCode === 200) {
        return response;
    }
    if (response.statusCode === 401) {
        return response.statusCode;
    }
    return null;
}


/*
Purpose: send a request to a specified url with information encoded in the body
Inputs: Method, url, body information in JSON format, callback with the signature (error, response, body) to handle the response from the specified url
Outputs: None
*/
function sendRequestBody(method, uri, body_json, callback) {
    var options = {};
    options['headers'] = {'Content-Type': 'application/json'};
    options['uri'] = uri;
    options['method'] = method;
    options['body'] = body_json;
    options['json'] = true;
    request(options, callback);
}


/*
Purpose: send a GET or POST request with information set in the header
Inputs: Method, url, header in JSON format, callback with the signature (error, response, body)
Outputs: None
*/
function sendRequestHeader(method, uri, header_json, callback) {
    var options = {};
    options['uri'] = uri;
    options['method'] = method;
    options['headers'] = header_json;
    options.headers['content-type'] = 'application/json';
    request(options, callback);
}


/*
Purpose: handle login request
Input: request, response
Output: None
*/
function handleLogin(request, response) {
    var body = {'email': request.body.email, 'password': request.body.password};
    sendRequestBody('POST', 'http://localhost:5050/login', body, function(err, res, body) {
        if (res === undefined) {
            response.status(204);
            response.json({'message': 'Response from login is undefined. Check request structure.'});
            return null;
        }
        if (parseInt(res.statusCode) === 401) {
            response.status(401);
            response.json(body);
            return null;
        }
        if (res.body === undefined || res.body === null) {
            response.status(204);
            response.json({'message': 'No information present.'});
            return null;
        }
        response.status(200);
        response.json(body);
        return null;
    });
}


/*
Purpose: Checks to see that the user in question is authenticated
Input: request with jwt token included in the header, response, next
Output: None
*/
function isLoggedIn(request, response, next) {
    if (request.headers.token === null || request.headers.token === undefined) {
        response.status(401);
        response.json({'message': 'Invalid request.'});
        return null;
    }
    var header = {'token': request.headers.token};
    sendRequestHeader('GET', 'http://localhost:5050/validate', header, function (err, res, body) {
        if (parseInt(res.status) === 401) {
            res.redirect('/login');
            return null;
        }
        request.user = JSON.parse(res.body);
        next();
    });
}


module.exports.sendRequestBody = sendRequestBody;
module.exports.sendRequestHeader = sendRequestHeader;
module.exports.handleLogin = handleLogin;
module.exports.isLoggedIn = isLoggedIn;