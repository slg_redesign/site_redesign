/*
Setup
=====
Purpose: Setup the basic application requirements
Author(s): Greg Cho
Reviewer: None
Date Reviewed: None
Notes: None 
*/


var mysql = require('mysql');
var express = require('express');
var engines = require('consolidate');
var bodyParser = require('body-parser');
var http = require('http');

var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var port = process.env.PORT || 1200;

// set up body parser options
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

server.listen(port, function(){
    console.log('- Server listening on given port 1200');
});

module.exports.app = app;
module.exports.server = server;
module.exports.port = port;