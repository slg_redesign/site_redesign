/*
Server
======
Purpose: Set up server elements
Author(s): Greg Cho
Reviewer(s): None
Date Reviewed: None 
Notes: None
*/


// requirements
var setup = require('./server_elements/setup/setup');
var subroutines = require('./server_elements/subroutines/subroutines');

// login functionality
setup.app.post('/login', subroutines.handleLogin);

// test for middleware
setup.app.get('/valid', subroutines.isLoggedIn, function(request, response) {
	response.json(request.user);
	return null;
});