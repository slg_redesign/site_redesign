// Open ID
// =======
// Purpose: The purpose of this file is to provide the class OpenIdObj which sets up open id authentication for the given application
// Author: Greg Cho
// Reviewer: None
// Date Reviewed: None
// Comments: None


// Purpose: To set up open id authentication
// Parameters: application element, return url, realm (url)
// Author: Greg Cho
class OpenIdObj {


    constructor(app, returnURL, realm) {
        // required elements for AuthObj
        this.crypto = require('crypto'); // need this for hash function hence it is global
        this.returnURL = returnURL;
        this.realm = realm;
        var flash = require('connect-flash');
        var cookie_parser = require('cookie-parser');
        var body_parser = require('body-parser');

        // for scoping purposes
        var hash = this.hash;
        var createUser = this.createUser;
        var cons = this;

        // passport setup elements
        this.passport = require('passport');
        this.OpenIdStrategy = require('passport-openid').Strategy;

        // mysql elements
        var mysql = require('mysql');
        var conn = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'nthgames2'
        });

        // create session parameters
        this.session = require('express-session');
        app.use(this.session({ // note that this is simply meant to indicate parameters
            secret: 'ASJFKDJSFLKr2urieowurdkfjsdlakKDFSDrewSucks', // secret key for session creation and cookie encryption
            resave: false,
            saveUninitialized: false,
            duration: 30 * 60 * 1000, // duration for the cookie
            activeDuration: 5 * 60 * 1000
        }));

        // more app setup
        app.use(flash());
        app.use(this.passport.initialize());
        app.use(this.passport.session());

        // passport strategies
        passport.use('openid', new OpenIdStrategy({
            returnURL: this.returnURL,
            realm: this.realm,
            profile: true
        },
        function(identifier, profile, done) {
            // TODO: Implement functionality to find or create a user based on the identifier.
            var user_sql = 'SELECT email, user_id, first_name, last_name, open_id FROM users WHERE email IN (';
            var user_sql = constructQuery.call(cons, user_sql, profile.emails);
            var user_sql = user_sql.substring(0, user_sql.length - 2) + 'OR open_id = ?;';
            console.log(user_sql); // for testing purposes
            conn.query(user_sql, [identifier], function(err, rows, fields) {
                if (parseInt(rows.length) === 0) { // if no user was found with matching emails
                    createUser.call(cons, profile.emails[0].value, profile.name.givenName, profile.name.familyName, null, identifier, conn, done);
                } else {
                    done(null, rows[0]);
                }
            });
        }));
    }


    // Purpose: Create a user entry in the user table
    // Input: email, first name, last name, password, open id, connection, done function
    // Output: None, the function calls the done callback function with the user information after completion
    // Note: This function does NOT check the the user element being inserted doesn't exist before hand. This logic
    createUser(email, first_name, last_name, password, open_id, conn, done) {
        var sql_create = 'INSERT INTO users (email, first_name, last_name, password, open_id) VALUES (?, ?, ?, ?, ?);';
        conn.query(sql_create, [email, first_name, last_name, password, open_id], function(err, rows, fields){
            if (err) return done(err);
            var sql_user = 'SELECT email, user_id, password, open_id FROM users WHERE email = ?;';
            conn.query(sql_user, [email], function(err2, rows2, fields2) {
                if (err2) return done(err2);
                done(null, rows2[0]);
            });
        });
    }


    // Purpose: Simple function to construct query from the iterable element
    // Input: sql query, iterable element to be appended to said query
    // Output: sql query with appended elements
    constructQuery(sql, iterable) {
        // note that the sql query must be of the form 'WHERE column IN ('.
        for (i = 0; i < iterable.length; i++) { // iterate through teh iterable
            var item = iterable[i].value; // retrieve the item in question
            if (i <= iterable.length - 2) { // if the item in question is not the last item in the iterable
                sql = String(sql) + String(item) + ', '; // ensure and enforce type match
            } else { // different append structure for the last item
                sql = String(sql) + String(item) + ');'; // ensure and enforce type match.
            }
        }
        return sql; // return the end sql query
    }


    // Purpose: function to check to see if the request maintains a session
    // Input: request, response, next function
    // Outputs: None
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) { // checks to see if the request maintains a cookie with user and passport key element
            return next();
        }
        // if the request doesn't maintain a session
        res.redirect('/login'); // redirect to login page
    }


    // Purpose: function to check to see if the request isn't logged in
    // Input: request, response, next function
    // Output: None
    notLoggedIn(req, res, next) {
        if (!req.isAuthenticated()) { // inverse of the above
            return next();
        }
        // if the request is logged in
        res.redirect('/') // redirect to the home page
    }


    // Purpose: Create a special token for verification
    // Input: Length of the desired token
    // Output: Token
    generateToken(length) {
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        var result = '';
        for (i = 0; i < length + 1; i++) {
            result += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return result;
    }

}