// JWT Authentication:
// ===================
// Purpose: Sets up JWT Authentication Class
// Author(s): Greg Cho
// Reviewer: None
// Date Reviewed: None
// Comments: The class is a bit long... Sorry, I want each class to be independent of each other, they all accomplish a different method of authentication.


// Purpose: Set up JWT token based authentication functionality
// Parameters: app
// Class functions: tokenPayload, hash, findOrCreateUser, authenticateUser, jwtLoginMiddleware, jwtRegistrationMiddleware, modifySecret, (additional mutators and accessors for db settings)
class JwtAuthObj {


    constructor(app) {
        // for internal scoping purposes
        var cons = this;

        // for external scoping purposes
        this.hash = this.hash.bind(this);
        this.findOrCreateUser = this.findOrCreateUser.bind(this);
        this.acceptOrRejectUser = this.acceptOrRejectUser.bind(this);
        this.jwtLogin = this.jwtLogin.bind(this);
        this.jwtRegistration = this.jwtRegistration.bind(this);
        this.tokenPayload = this.tokenPayload.bind(this);
        this.isLoggedIn = this.isLoggedIn.bind(this);
        this.notLoggedIn = this.notLoggedIn.bind(this);
        this.tokenPayloadAsync = this.tokenPayloadAsync.bind(this);
        this.modifyDbHost = this.modifyDbHost.bind(this);
        this.modifyDbName = this.modifyDbName.bind(this);
        this.modifyDbUser = this.modifyDbUser.bind(this);
        this.modifySecret = this.modifySecret.bind(this);
        this.modifyUserTable = this.modifyUserTable.bind(this);
        this.refreshToken = this.refreshToken.bind(this);
        this.changeUserValidationToken = this.changeUserValidationToken.bind(this);
        this.validDecoded = this.validDecoded.bind(this);
        this.getDbHost = this.getDbHost.bind(this);
        this.getDbName = this.getDbName.bind(this);
        this.getDbUser = this.getDbUser.bind(this);
        this.getDbPassword = this.getDbPassword.bind(this);
        this.getUserTableName = this.getUserTableName.bind(this);
        this.createJWT = this.createJWT.bind(this);
        this.changeUserPassword = this.changeUserPassword.bind(this);

        this.secret = 'W31c0me*T0*Sup3r*L3ague_Gaming'; // secret key for jwt token generation

        //required elements for jwtAuthObj
        this.passport = require('passport');
        this.crypto = require('crypto'); // need this for hash function hence it is global (depreciated, yse bcrypt)
        this.bcrypt = require('bcrypt');
        this.JwtStrategy = require('passport-jwt').Strategy;
        this.ExtractJwt = require('passport-jwt').ExtractJwt;
        this.LocalStrategy = require('passport-local').Strategy;
        this.jwt = require('jsonwebtoken');
        var bodyParser = require('body-parser');

        // mysql configuration
        this.user_table = 'users';
        this.mysqlConfig = {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'nthgames2'};

        // mysql elements to connect to database (edit as needed)
        this.mysql = require('mysql');
        this.conn = this.mysql.createConnection(this.mysqlConfig);

        // connect to the mysql database
        this.conn.connect(function(err) {
            if (err) throw err;
        });

        // configure jwt options
        this.jwtOptions = {}; // options object
        this.jwtOptions.secretOrKey = this.secret; // Input the secret key from the class constructor for validation of jwt token
        this.salt_rounds = 10;

        // NOTE: logout now needs to be handled on the front end as the token needs to be destroyed from client storage

    }


    createJWT(user, response) {
        var cons = this;
        if (parseInt(user.length) > 0) {
            var payload = {'user_id': user[0].user_id, 'email': user[0].email, 'first_name': user[0].first_name, 'last_name': user[0].last_name, 'validation_token': user[0].validation_token};
            var token = cons.jwt.sign(payload, cons.jwtOptions.secretOrKey, {expiresIn: '2 days', issuer: 'superleague.com'});
            response.json({'message': 'JWT token creation successful.', 'token': token});
        } else {
            response.json({'message': 'Database select error. No user found with the given credentials.'});
        }
    }


    // Purpose: Check to ensure that the decoded element maintains the correct information
    // Input: decoded jwt token
    // Output: callback will call with the decoded element if it's valid and null if it isn't
    validDecoded(decoded, callback) {
        var cons = this;
        var conn = this.conn;
        if (decoded === null || decoded === undefined) {
            callback(null);
        }
        if (decoded.user_id === undefined || decoded.validation_token === undefined) {
            callback(null);
        }
        var sql_check = "SELECT * FROM " + cons.user_table + " WHERE user_id = ? AND validation_token = ?;";
        conn.query(sql_check, [decoded.user_id, decoded.validation_token], function(err, rows, fields) {
            if (err) throw err;
            if (parseInt(rows.length) <= 0) {
                callback(null);
                return null;
            }
            callback(decoded);
            return null;
        });
    }


    // Purpose: Retrieve payload from jwt token after verification
    // Input: jwt token
    // Output: jwt payload if the token is valid, null if it isn't
    tokenPayload(token) {
        var cons = this;
        cons.jwt.verify(token, cons.jwtOptions.secretOrKey, function(err, decoded) {
            if (err) throw (err);
            cons.validDecoded(decoded, function(valid_user) {
                return valid_user;
            });
        });
    }


    // Purpose: Retrieve payload from jwt token after verification to be passed to a callback. This is meant to be used in an asynchronous fashion.
    // Input: jwt token, callback function
    // Output: None, callback function is called with the data
    tokenPayloadAsync(token, callback) {
        var cons = this;
        cons.jwt.verify(token, cons.jwtOptions.secretOrKey, function(err, decoded) {
            if (err) {
                callback(null); // still want the function to pass to the callback even in the event of an error
                return null;
            }
            cons.validDecoded(decoded, callback);
            return null;
        });
    }


    // Purpose: Create a special token for verification
    // Input: Length of the desired token
    // Output: Token
    generateValidationToken(length) {
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        var result = '';
        for (var i = 0; i < length + 1; i++) {
            result += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return result;
    }


    // Purpose: Change the user validation token
    // Input: user id, response (to send back the necessary JSON)
    // Output: A new token with the updated validation token in it's payload
    changeUserValidationToken(user_id, response) {
        var cons = this;
        var conn = this.conn;
        var sql_change = 'UPDATE ' + cons.user_table + ' SET validation_token = ? WHERE user_id = ?;';
        var sql_user = 'SELECT email, user_id, first_name, last_name, validation_token FROM ' + cons.user_table + ' WHERE user_id = ?;';
        var validation_token = cons.generateValidationToken(11);
        conn.query(sql_change, [validation_token, user_id], function(err, rows, fields) {
            if (err) throw err;
            conn.query(sql_user, [user_id], function(user_err, user, user_fields) {
                if (user_err) throw user_err;
                if (parseInt(user.length) > 0) {
                    var payload = {'user_id': user[0].user_id, 'email': user[0].email, 'first_name': user[0].first_name, 'last_name': user[0].last_name, 'validation_token': user[0].validation_token};
                    var token = cons.jwt.sign(payload, cons.jwtOptions.secretOrKey, {expiresIn: '2 days', issuer: 'superleague.com'});
                    response.json({'message': 'Validation token change successful.', 'token': token});
                } else {
                    response.json({'message': 'Database select error. No user found with the given credentials.'});
                }
            });
        });
    }


    // Purpose: Change user password
    // Inputs: user id, new password, response
    // Outputs: JSON element
    changeUserPassword(user_id, new_password, response) {
        var conn = this.conn;
        var cons = this;
        var sql_user = 'SELECT email, user_id, first_name, last_name, validation_token FROM ' + cons.user_table + ' WHERE user_id = ?;';
        var sql_update = 'UPDATE ' + cons.user_table + ' SET password = ?, validation_token = ? WHERE user_id = ?;';
        conn.query(sql_user, [user_id], function(err, rows, fields) {
            if (err) throw err;
            if (parseInt(rows.length) > 0) {
                var h = cons.hash(new_password, cons.salt_rounds); // hash the password
                var v_token = cons.generateValidationToken(11); // this may change
                conn.query(sql_update, [h, v_token, user_id], function(err, rows, fields) {
                    if(err) throw err;
                    conn.query(sql_user, [user_id], function(err, user, fields) {
                        if (err) throw err;
                        cons.createJWT(user, response);
                    });
                });
            }
        });
    }


    // Purpose: Hash a value with an inputted salt using the predetermined secret key.
    // Input: value to hash, salt rounds
    // Output: Resulting cypher with the salt, hash cost, and hash concatenated as one
    hash(value, salt_rounds = this.salt_rounds) {
        var hash = this.bcrypt.hashSync(value, salt_rounds);
        return hash;
    }


    // Purpose: Find or create a single user and call a callback function with the resulting user id
    // Input: Connection element, email, password, first name, last name, hash function with the appropriate signature (element, salt), done/next callback
    // Output: In the response the function writes a token and message in JSON format upon successful execution
    findOrCreateUser(res, email, password, first_name, last_name) {
        var conn = this.conn;
        var cons = this; // constructor scope
        var sql_user = 'SELECT email, user_id, password, first_name, last_name, validation_token FROM ' + cons.user_table + ' WHERE email = ?;'; // query to retrieve user information
        var sql_insert = 'INSERT INTO ' + cons.user_table + ' (email, password, first_name, last_name, validation_token) VALUES (?, ?, ?, ?, ?);'; // query to insert user information
        conn.query(sql_user, [email], function(err, rows, fields) { // execute query
            if (err) throw err;
            if (parseInt(rows.length) === 0) { // if there are no entries that match
                var h = cons.hash(password, cons.salt_rounds); // hash the password
                var v_token = cons.generateValidationToken(11); // this may change
                conn.query(sql_insert, [email, h, first_name, last_name, v_token], function(err2, rows2, fields2) {
                    if (err2) throw err2; // if there is an error that occurs in the second query catch it
                    conn.query(sql_user, [email], function(err3, rows3, fields3) { // retrieve newly created user
                        if (err3) throw err3;
                        if (parseInt(rows3.length) > 0) {
                            var payload = {'user_id': rows3[0].user_id, 'email': rows3[0].email, 'first_name': rows3[0].first_name, 'last_name': rows3[0].last_name, 'validation_token': rows3[0].validation_token};
                            var token = cons.jwt.sign(payload, cons.jwtOptions.secretOrKey, {expiresIn: '2 days', issuer: 'superleague.com'});
                            res.json({message: 'Registration successful.', token: token});
                        } else {
                            res.json({message: 'Database insert error.'});
                        }
                    });
                });
            } else {
                res.json({message: 'Email already used in a previous account.'});
            }
        });
    }


    // Purpose: Find or reject a user should their credentials be valid
    // Input: Connection element, request, email, password, done/next callback
    // Output: None
    acceptOrRejectUser(res, email, password) {
        var conn = this.conn;
        var cons = this; // constructor scope
        // gets user information based on email
        var sql_user = 'SELECT email, user_id, first_name, last_name, password, validation_token FROM ' + this.user_table + ' WHERE email=?;';
        conn.query(sql_user, [email], function(err, rows, fields) { // execute the sql request based on email
            if (err) throw err;
            if (parseInt(rows.length) === 0) { // determine if email is invalid
                res.status(401).json({message:"Invalid email."});
            } else {
                cons.bcrypt.compare(password, rows[0].password, function(err, result) {
                    if (err) throw err;
                    if (result) {
                        var payload = {'user_id': rows[0].user_id, 'email': rows[0].email, 'first_name': rows[0].first_name, 'last_name': rows[0].last_name, 'validation_token': rows[0].validation_token};
                        var token = cons.jwt.sign(payload, cons.jwtOptions.secretOrKey, {expiresIn: '2 days', issuer: 'superleague.com'});
                        res.json({message: "Login successful.", token: token});
                    } else {
                        res.status(401).json({message:"Invalid password."});
                    }
                });
            }
        });
    }


    // Purpose: Login an individual using jwt as a middleware strategy
    // Input: Request, response, next callback
    // Output: None (calls the next function)
    jwtLogin(req, res) {
        this.acceptOrRejectUser(res, req.body.email, req.body.password);
    }


    // Purpose: Registration strategy
    // Input: Request, response, next callback (note that the request is expected to have email, password, first_name, and last_name as parameters)
    // Output: None (calls the next function)
    jwtRegistration(req, res) {
        var email = req.body.email;
        var password = req.body.password;
        var first_name = req.body.first_name;
        var last_name = req.body.last_name;
        var captcha = req.body.captcha; // TODO: implement captcha mechanism
        if (email === null || password === null || first_name === null || last_name === null ) {
            res.json({'message': 'Invalid inputs. One or more arguments are null.'});
        }
        this.findOrCreateUser(res, email, password, first_name, last_name);
    }


    // Purpose: Check to see if the user is in fact logged in
    // Input: request, response, next
    // Output: None, should call next if the use is logged in and simply redirect the user if they aren't
    isLoggedIn(req, res, next) {
        if (req.headers === undefined || req.headers === null) {
            res.status(401).json({"message": "Missing header element."});
            res.end();
            return null;
        }
        if (req.headers.token === undefined || req.headers.token === null) {
            res.status(401).json({"message": "Token not present/detected."});
            res.end();
            return null;
        }
        var token = req.headers.token;
        this.tokenPayloadAsync(token, function(user) {
            if (user === null || user === undefined) {
                res.status(401).json({"message": "Token provided is invalid."});
                res.end();
                return null;
            }
            req.user = user;
            next();
            return null;
        });
    }


    // Purpose: Refresh token should it be expired
    // Input: request, response
    // Output: None, the new token should be sent via JSON to the client
    refreshToken(req, res) {
        var cons = this;
        var token = null;

        // check to see that the request actually has the properties required
        if (req !== null && req.headers !== null) {
            token = req.headers.token;
            // retrieve profile information from token
            cons.jwt.verify(token, cons.jwtOptions.secretOrKey, function(err, decoded) {
                var date = new Date();
                var utc = date.getTime(); // gives the time since some specified date in milliseconds (note: jwt uses the same UTC time in seconds)
                if (err) {
                    res.status(401);
                    res.json({'message': 'Token not valid.'});
                    return null;
                }

                // ensure the token isn't too old
                if ((utc / 1000) - decoded.exp > 2) {
                    res.status(401);
                    res.json({'message': 'Token expired too long ago.'});
                    return null;
                }

                // check to see if a token refresh is necessary
                if (decoded.exp - (utc / 1000) > 0) {
                    // console.log('Ques custodiet ipsus custodes.');
                    res.json({'message': 'Token still valid; refresh not required.', 'token': token});
                    return null;
                } else {
                    // issue a new token
                    var payload = {'user_id': decoded.user_id, 'email': decoded.email, 'first_name': decoded.first_name, 'last_name': decoded.last_name, 'validation_token': decoded.validation_token};
                    var refreshed_token = cons.jwt.sign(payload, cons.jwtOptions.secretOrKey, {expiresIn: '2 days'});
                    res.json({'message': 'Token refresh successful.', 'token': refreshed_token});
                    return null;
                }
            });
        }
    }


    // Purpose: does the exact opposite of isLoggedIn
    // Input: request, response, next
    // Output: None
    // TODO: implement logic for this
    notLoggedIn(req, res, next) {
        if (req.headers === undefined || req.headers === null) {
            next();
            return null;
        }
        if (req.headers.token === undefined || req.headers.token === null) {
            next();
            return null;
        }
        var token = req.headers.token;
        this.tokenPayloadAsync(token, function(user) {
            if (user === null || user === undefined) {
                next();
                return null;
            }
            res.json({'message': 'user already logged in. Current resource unavailable for authenticated users.'});
            return null;
        });
    }


    // Purpose: Modify secret key for token creation using JWT
    // Input: New secret key
    // Output: None
    modifySecret(new_secret) {
        this.secretOrKey = new_secret;
        return true;
    }


    // Purpose: Mutate mysql configuration for host
    // Input: New Host
    // Output: None
    modifyDbHost(new_host) {
        this.mysqlConfig.host = new_host;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        this.conn.connect(function(err) {
            if (err) throw err;
        });
        return true;
    }


    // Purpose: Mutator for DB connection settings
    // Input: New database name
    // Output: None
    modifyDbName(new_db) {
        this.mysqlConfig.database = new_db;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        this.conn.connect(function(err) {
            if (err) throw err;
        });
        return true;
    }


    // Purpose: Mutator for database user connection setting
    // Input: New user name
    // Output: None
    modifyDbUser(new_user) {
        this.mysqlConfig.user = new_user;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        this.conn.connect(function(err) {
            if (err) throw err;
        });
        return true;
    }


    // Purpose: Mutator for modifying database password
    // Input: New password
    // Output: None
    modifyDbPassword(new_pass) {
        this.mysqlConfig.password = new_pass;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        this.conn.connect(function(err) {
            if (err) throw err;
        });
        return true;
    }


    // Purpose: Mutator to change user table identifier
    // Input: New table name
    // Output: None
    modifyUserTable(new_user_table) {
        this.user_table = new_user_table;
    }


    // Purpose: get db host
    // Inputs: None
    // Outputs: current mysql host
    getDbHost() {
        return this.mysqlConfig.host;
    }

    
    // Purpose: get db name
    // Inputs: None
    // Output: database name
    getDbName() {
        return this.mysqlConfig.database;
    }


    // Purpose: get user name used in database
    // Inputs: None
    // Outputs: user name
    getDbUser() {
        return this.mysqlConfig.user;
    }


    // Purpose: get the database password
    // Inputs: None
    // Outputs: the database password
    getDbPassword() {
        return this.mysqlConfig.password;
    }


    // Purpose: get user table name
    // Inputs: None
    // Outputs: user table name
    getUserTableName() {
        return this.user_table;
    }

}


module.exports.JwtAuthObj = JwtAuthObj;
