// Authenticate
// =====
// Purpose: Setup basic requirements for server and all related elements using AuthOb.
// Author(s): Greg Cho
// Reviewer: None
// Date Reviewed: None
// Comments: None


class AuthObj {
    // Class parameters: app
    // Class functions: hash, isLoggedIn, notLoggedIn
    // Class purpose: The AuthObj class edits the given app object as necessary and sets up passport local strategies to be used by the instantiating entity. As AuthObj is mostly setting up local strategies and importing requirements most actions tak place in the class constructor (i.e. changes are executed upon instantiation).
    // Author(s): Greg Cho, Chris Weber


    constructor(app) {
        // setup app variable in class constructor
        // this.app = app;

        // required elements for AuthObj
        this.crypto = require('crypto'); // need this for hash function hence it is global (depreciated, use bcrypt)
        this.bcrypt = require('bcrypt');
        var flash = require('connect-flash');
        var cookie_parser = require('cookie-parser');
        var body_parser = require('body-parser');

        // for internal scoping purposes
        var hash = this.hash.bind(this);
        var findOrCreateUser = this.findOrCreateUser.bind(this);
        var acceptOrRejectUser = this.acceptOrRejectUser.bind(this);
        var cons = this;

        // for external scoping purposes (must bind the function calls to the scope of the constructor)
        this.hash = this.hash.bind(this);
        this.findOrCreateUser = this.findOrCreateUser.bind(this);
        this.acceptOrRejectUser = this.acceptOrRejectUser.bind(this);
        this.modifyDbHost = this.modifyDbHost.bind(this);
        this.modifyDbName = this.modifyDbName.bind(this);
        this.modifyDbUser = this.modifyDbUser.bind(this);

        // passport setup elements
        this.passport = require('passport');
        this.LocalStrategy = require('passport-local').Strategy;

        // mysql configuration
        this.mysqlConfig = {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'nthgames2'};

        // mysql elements to connect to database (edit as needed)
        this.mysql = require('mysql');
        this.conn = this.mysql.createConnection(this.mysqlConfig);

        // connect to the mysql database
        this.conn.connect(function(err) {
            if (err) throw err;
        });

        // create session parameters
        this.session = require('express-session');
        app.use(this.session({ // note that this is simply meant to indicate parameters
            secret: 'ASJFKDJSFLKr2urieowurdkfjsdlakKDFSDrewSucks', // secret key for session creation and cookie encryption
            resave: false,
            saveUninitialized: false,
            duration: 30 * 60 * 1000, // duration for the cookie
            activeDuration: 5 * 60 * 1000
        }));

        // more app setup
        app.use(flash());
        app.use(this.passport.initialize()); // inialize passport
        app.use(this.passport.session());

        // local strategies for passport
        // passport local login strategy
        this.passport.use('local-login', new this.LocalStrategy({
            passReqToCallback : true,
            usernameField : 'email',
            passwordField : 'password'},
            function(req, email, password, done) {
                cons.acceptOrRejectUser(req, email, password, hash, done);
            }
        ));

        // passport local sign up strategy
        this.passport.use('local-signup', new this.LocalStrategy({
            passReqToCallback: true,
            usernameField : 'email',
            passwordField : 'password'
        }, function(req, email, password, done) {
            // console.log(req);
            var password = req.body.password;
            var email = req.body.email;
            var first_name = req.body.first_name;
            var last_name = req.body.last_name;
            var captcha = null;
            cons.findOrCreateUser(req, email, password, first_name, last_name, hash, done); // called from constructor view
        }));

        // serialization functionality (saves user id to passport element)
        this.passport.serializeUser(function(user, done) {
            done(null, user.user_id);
        });

        // stores user information on the cookie
        this.passport.deserializeUser(function(id, done) {
            var sql_user = 'SELECT user_id, email, first_name, last_name FROM users WHERE user_id = ?;'; // get user information via user id
            cons.conn.query(sql_user, [id], function(err, rows, fields) {
                if (parseInt(rows.length) === 0) {
                    return done(null, false);
                }
                return done(null, rows[0]);
            });
        });

        // logout functionality
        app.get('/logout', function(req, res) {
            req.logout();
            res.redirect('/');
        });

        this.salt_rounds = 10;
    }


    // Purpose: Hash a value with an inputted salt using the predetermined secret key.
    // Input: value to hash, salt rounds
    // Output: Result hash
    hash(value, salt_rounds = this.salt_rounds) {
        var hash = this.bcrypt.hashSync(value, salt_rounds);
        return hash;
    }


    // Purpose: function to check to see if the request maintains a session
    // Input: request, response, next function
    // Outputs: None
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) { // checks to see if the request maintains a cookie with user and passport key element
            return next();
        }
        // if the request doesn't maintain a session
        res.redirect('/login'); // redirect to login page
    }


    // Purpose: function to check to see if the request isn't logged in
    // Input: request, response, next function
    // Output: None
    notLoggedIn(req, res, next) {
        if (!req.isAuthenticated()) { // inverse of the above
            return next();
        }
        // if the request is logged in
        res.redirect('/') // redirect to the home page
    }


    // Purpose: Create a special token for verification
    // Input: Length of the desired token
    // Output: Token
    generateToken(length) {
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        var result = '';
        for (var i = 0; i < length + 1; i++) {
            result += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return result;
    }


    // Purpose: Find or create a single user and call a callback function with the resulting user id
    // Input: Connection element, request, email, password, first name, last name, hash function with the appropriate signature (element, salt), done callback
    // Output: None
    findOrCreateUser(req, email, password, first_name, last_name, hash_function, done) {
        var conn = this.conn;
        var cons = this;
        var sql_user = 'SELECT email, user_id, password FROM users WHERE email = ?;'; // query to retrieve user information
        var sql_insert = 'INSERT INTO users (email, password, first_name, last_name) VALUES (?, ?, ?, ?);'; // query to insert user information
        conn.query(sql_user, [email], function(err, rows, fields) { // execute query
            if (err) return done(err);
            if (parseInt(rows.length) === 0) { // if there are no entries that match
                var h = hash_function(password, cons.salt_rounds); // hash the password 
                conn.query(sql_insert, [email, h, first_name, last_name], function(err2, rows2, fields2) {
                    if (err2) return done(err2); // if there is an error that occurs in the second query catch it
                    conn.query(sql_user, [email], function(err3, rows3, fields3) { // retrieve newly created user
                        if (err3) return done(err3);
                        if (parseInt(rows3.length) > 0) {
                            //Here we need to calculate email token for user, add it to the database, and send them an email.
                            return done(null, rows3[0]); // send user object to the done function
                        } else {
                            return done(null, false, {message: 'Database insert error.'});
                        }
                    });
                });
            } else {
                return done(null, false, {message: 'Email already used in a previous account.'});
            }
        });
    }  


    // Purpose: Find or reject a user should their credentials be valid
    // Input: Connection element, request, email, password, hash function, done callback
    // Output: None
    acceptOrRejectUser(req, email, password, hash_function, done) {
        var conn = this.conn;
        var cons = this;
        // gets user information based on email
        var sql_user = 'SELECT email, user_id, password FROM users WHERE email=?;';
        conn.query(sql_user, [email], function(err, rows, fields) { // execute the sql request based on email
            if (err) return done(err);
            if (parseInt(rows.length) === 0) { // determine if email is invalid
                return done(null, false, {message: 'Invalid email.'});
            } else {
                cons.bcrypt.compare(password, rows[0].password, function(err, res) {
                    if (err) done(err);
                    if (res) {
                        done(null, rows[0]);
                    } else {
                        done(null, false, {message: 'invalid password.'});
                    }
                });
            }
        });
    }


    // Purpose: Mutate mysql configuration for host
    // Input: New Host
    // Output: None
    modifyDbHost(new_host) {
        this.mysqlConfig.host = new_host;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        return null;
    }


    // Purpose: Mutator for DB connection settings
    // Input: New database name
    // Output: None
    modifyDbName(new_db) {
        this.mysqlConfig.database = new_db;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        return null;
    }


    // Purpose: Mutator for database user connection setting
    // Input: New user name
    // Output: None
    modifyDbUser(new_user) {
        this.mysqlConfig.user = new_user;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        return null;
    }


    // Purpose: Mutator for modifying database password
    // Input: New password
    // Output: None
    modifyDbPassword(new_pass) {
        this.mysqlConfig.password = new_pass;
        this.conn = this.mysql.createConnection(this.mysqlConfig);
        return null;
    }


    // TODO: add accessors for database setting information

}


module.exports.AuthObj = AuthObj; // export AuthObj
